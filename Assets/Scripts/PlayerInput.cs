﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public PlayerMovement movement;
    public SwordAnimatorInyector swordAnimatorInyector;

    public Vector2 minMaxTurnAmountRandom;
    public float turnOrderAmount = 30f;
    private void Awake()
    {
        movement = GetComponent<PlayerMovement>();
        swordAnimatorInyector = GetComponentInChildren<SwordAnimatorInyector>();
    }

    public float maxStopCooldown = 15f;
    public float currentStopCooldown;

  
    public void Update()
    {
        if (currentStopCooldown > 0)
        {
            currentStopCooldown -= Time.deltaTime;
            currentStopCooldown = Mathf.Clamp(currentStopCooldown, 0, maxStopCooldown);
            StopUIManager.Instance.SetNormalizedValue(1 - (currentStopCooldown / maxStopCooldown));
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            Test_LeftTurn();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            Test_RightTurn();
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            Test_Faster();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Test_Slower();
        }
        if (Input.GetMouseButtonDown(0))
        {
            Test_Attack();
        }
        if (Input.GetMouseButtonDown(1))
        {
            Test_Block();
        }
        if (Input.GetKeyDown(KeyCode.Space) && currentStopCooldown == 0)
        {
            Test_STOP();
        }
    }

    public void Test_STOP()
    {
        PostProcessManager.Instance.StopFX();        
        var pm = GetComponent<PlayerMovement>();
        pm.Stop();
        CommandManager.Instance.ClearAllCommands(true);
        CommandManager.Instance.StopCurrentCommand();
        PostProcessManager.Instance.SlowDownEffect();
        currentStopCooldown = maxStopCooldown;
        AudioController.Instance.PlayVOStop();
        swordAnimatorInyector.PlayMouthAnimation("LONG");
    }

    public void Test_Faster()
    {
        ActionCommand speedCommand = new AddSpeedCommand(movement, 1);
        speedCommand.UItexts = new List<string> { "Go faster dammit!" };
        //Mover esta mierda a un scriptable
        CommandManager.Instance.QueueCommand(speedCommand);
        AudioController.Instance.PlayVOAccelerate();
        swordAnimatorInyector.PlayMouthAnimation("DOUBLE");
    }

    public void Test_Slower()
    {
        ActionCommand speedCommand = new AddSpeedCommand(movement, -1);
        speedCommand.UItexts = new List<string> { "You are going too fast!" , "Tread carefully now, minion" , "Slow down!"};
        CommandManager.Instance.QueueCommand(speedCommand);
        AudioController.Instance.PlayVOBrake();
        swordAnimatorInyector.PlayMouthAnimation("SHORT");
    }


    public void Test_LeftTurn()
    {
        ActionCommand turnCommand = new TurnCommand(movement, -turnOrderAmount +Random.Range(minMaxTurnAmountRandom.x, minMaxTurnAmountRandom.y));
        turnCommand.UItexts = new List<string> { "Turn left!" , "LEFT!!", "Turn left my minion!", "To the left now!" };
        CommandManager.Instance.QueueCommand(turnCommand);
        AudioController.Instance.PlayVOLeft();
        swordAnimatorInyector.PlayMouthAnimation("SHORT");
    }

    public void Test_RightTurn()
    {
        ActionCommand turnCommand = new TurnCommand(movement, turnOrderAmount + Random.Range(minMaxTurnAmountRandom.x, minMaxTurnAmountRandom.y));
        turnCommand.UItexts = new List<string> { "Turn to the right!", "The other left!", "C'mon, turn right!" };
        CommandManager.Instance.QueueCommand(turnCommand);
        AudioController.Instance.PlayVORight();
        swordAnimatorInyector.PlayMouthAnimation("SHORT");
    }

    public void Test_Attack()
    {
        ActionCommand attackCommand = new AttackCommand(movement, turnOrderAmount);
        attackCommand.UItexts = new List<string> { "Wield my power!", "Cut them down!", "ATTAAAAAAACK!!", "Attack, now!!" };
        CommandManager.Instance.QueueCommand(attackCommand);
        AudioController.Instance.PlayVOAttack();
        swordAnimatorInyector.PlayMouthAnimation("DOUBLE");
    }

    public void Test_Block()
    {
        BlockCommand blockCommand = new BlockCommand(movement, turnOrderAmount);
        blockCommand.UItexts = new List<string> { "Block!!", "Defend yourself!", "Do not tarry and parry!", "Full defense minion!" };
        CommandManager.Instance.QueueCommand(blockCommand);
        //AudioController.Instance.PlayVOAttack();
        swordAnimatorInyector.PlayMouthAnimation("DOUBLE");
    }
}

