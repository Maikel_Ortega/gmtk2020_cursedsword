﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerMovement : MonoBehaviour
{
    [Header("ACTIONS")]
    public float secondsPerAngle= 0.05f;
    public float secondsToChangeSpeed = 0.5f;

    [Space()]
    [Header("MOVEMENT")]
    public float maxSpeed = 5f;
    public float speed = 0f;
    public CharacterController ccontroller;


    [Space()]
    [Header("ATTACK")]
    public AttackArea attackArea;[Space()]
    public Animator swordAnimator;


    [Space()]
    [Header("DEBUG")]
    public Vector3 externalVelocity;

    bool isProcessingMovement = false;
    Vector3 velocity;

    public void Stop()
    {
        velocity = Vector3.zero;
        SetSpeed(0);
    }

    private void Update()
    {

        velocity = transform.forward * speed;
        if (externalVelocity.magnitude == 0)
        { 
            velocity.y = -9.8f;
        }
        ccontroller.Move(velocity*Time.deltaTime);
    }

    void LateUpdate()
    {
        ccontroller.Move(externalVelocity*Time.deltaTime);
    }

    public void Turn(float angle)
    {
        isProcessingMovement = true;
        Vector3 addRotation = new Vector3(0, angle,0);
        transform.DORotate(addRotation, Mathf.Abs(angle) * secondsPerAngle, RotateMode.WorldAxisAdd).onComplete = TweenCallback;
    }

    public void TweenCallback()
    {
        isProcessingMovement = false;
    }

    public bool IsProcessingMovement()
    {
        return isProcessingMovement;
    }

    public void AddSpeed(float floatParam)
    {
        isProcessingMovement = true;
        float targetSpeed = Mathf.Clamp(speed +floatParam, -2, maxSpeed);
        DOTween.To(GetCurrentSpeed, SetSpeed, targetSpeed, secondsToChangeSpeed).onComplete = TweenCallback;

    }

    internal void Teleport(Transform point)
    {
        ccontroller.enabled = false;
        transform.position = point.transform.position;
        transform.rotation = point.transform.rotation;
        ccontroller.enabled = true;
    }    

    private void SetSpeed(float s)
    {
        speed = s;
    }

    private float GetCurrentSpeed()
    {
        return speed;
    }

    public float blockDuration = 1f;
    internal void Block(float floatParam)
    {
        isProcessingMovement = true;
        StartCoroutine(BlockCoroutine());
    }

    IEnumerator BlockCoroutine()
    {
        swordAnimator.SetBool("BLOCK", true);
        DamageReceiver dr = GetComponentInChildren<DamageReceiver>();
        dr.blockDamage = true;
        yield return new WaitForSeconds(blockDuration);
        dr.blockDamage = false;
        swordAnimator.SetBool("BLOCK", false);
        isProcessingMovement = false;
    }

    public void Attack(float floatParam)
    {
        isProcessingMovement = true;
        //ATTACK ANIMATION
        swordAnimator.SetTrigger("ATTACK");
        StartCoroutine(AttackCoroutine());
    }

    IEnumerator AttackCoroutine()
    {
        yield return new WaitForSeconds(0.4f);
        attackArea.Attack(gameObject, 0.15f);
        yield return new WaitForSeconds(0.8f);
        isProcessingMovement = false;
    }

}
