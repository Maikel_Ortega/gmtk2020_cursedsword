﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionCommand
{
    public ActionCommand(PlayerMovement target, float floatParam)
    {
        this.floatParam = floatParam;
        this.target = target;
    }

    public PlayerMovement target;
    public float floatParam;
    public float delay=0.3f;
    public string name = "unnamedCommand";
    public List<string> UItexts;
    public bool IsRunning = false;

    public virtual IEnumerator Evaluate() { yield return null;}
    public event System.Action<ActionCommand> OnCommandFinished;

    public void StartCommand(MonoBehaviour commandRunner)
    {
        IsRunning = true;
        commandRunner.StartCoroutine(Evaluate());
    }

    public string GetUiText()
    {
        return UItexts[UnityEngine.Random.Range(0, UItexts.Count)];
    }

    protected void CommandFinished()
    {
        IsRunning = false;
        OnCommandFinished?.Invoke(this);
    }

    public override string ToString()
    {
        return string.Format("{0} // Param: {1}", name, floatParam);
    }
}


public class AddSpeedCommand : ActionCommand
{    
    public AddSpeedCommand(PlayerMovement target, float floatParam) : base(target, floatParam)
    {
        name = "Change Speed";
    }

    public override IEnumerator Evaluate()
    {
        delay = 0.3f;
        yield return new WaitForSeconds(delay);
        target.AddSpeed(floatParam);
        while (target.IsProcessingMovement())
        {
            yield return null;
        }
        CommandFinished();
    }
}


public class BlockCommand : ActionCommand
{
    public BlockCommand(PlayerMovement target, float floatParam) : base(target, floatParam)
    {
        name = "Block";
    }

    public override IEnumerator Evaluate()
    {
        yield return new WaitForSeconds(delay);
        target.Block(floatParam);
        while (target.IsProcessingMovement())
        {
            yield return null;
        }
        CommandFinished();
    }
}

public class AttackCommand : ActionCommand
{
    public AttackCommand(PlayerMovement target, float floatParam) : base(target, floatParam)
    {
        name = "Attack";
    }

    public override IEnumerator Evaluate()
    {
        yield return new WaitForSeconds(delay);
        target.Attack(floatParam);
        while (target.IsProcessingMovement())
        {
            yield return null;
        }
        CommandFinished();
    }
}

public class TurnCommand : ActionCommand
{
    public TurnCommand(PlayerMovement target, float floatParam) : base(target, floatParam)
    {
        name = "Rotate";
    }

    public override IEnumerator Evaluate()
    {
        yield return new WaitForSeconds(delay);
        target.Turn(floatParam);
        while (target.IsProcessingMovement())
        {
            yield return null;
        }
        CommandFinished();
    }
}

public class StopCommand : ActionCommand
{
    public StopCommand(PlayerMovement target, float floatParam) : base(target, floatParam)
    {
        name = "STAHP";
    }

    public override IEnumerator Evaluate()
    {
        yield return null;
        CommandFinished();
    }
}