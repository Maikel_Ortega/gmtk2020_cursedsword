﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovingPlatformHelper : MonoBehaviour
{
    public LayerMask groundLayerMask;

    public Vector3 offset;
    public float range = 1f;

    public PlayerMovement pmovement;
    bool playerWasOn = false;

    private void Awake()
    {
        pmovement = GetComponent<PlayerMovement>();
    }

    void Update()
    {
        CheckGround();
    }

    void CheckGround()
    {
        RaycastHit info;
        if (Physics.Raycast(transform.position + offset, Vector3.down, out info, range, groundLayerMask))
        {
            Debug.DrawRay(transform.position + offset, Vector3.down * range, Color.green);
            var g = info.collider.gameObject.GetComponent<MovingPlatform>();
            if (!g)
            { 
                g= info.collider.gameObject.GetComponentInParent<MovingPlatform>();
            }
            if (g)
            { 
                pmovement.externalVelocity = g.GetVelocity();
                playerWasOn = true;
            }
        }
        else
        {
            if (playerWasOn)
            {
                playerWasOn = false;
                pmovement.externalVelocity = Vector3.zero;
            }
            Debug.DrawRay(transform.position + offset, Vector3.down * range, Color.red);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + offset, 0.1f);
        Gizmos.DrawLine(transform.position + offset, transform.position + offset + Vector3.down * range);
    }
}
