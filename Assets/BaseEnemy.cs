﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maikel.StatelessFSM;
using Plugins.Maikel.StateMachine;
using System;
using UnityEngine.AI;
using DG.Tweening;
using Random = UnityEngine.Random;

public class BaseEnemy : MonoBehaviour
{
    public StateMachine<BaseEnemy> fsm;
    public State<BaseEnemy> stIdle;
    public State<BaseEnemy> stChase;
    public State<BaseEnemy> stAttack;
    public State<BaseEnemy> stReposition;
    public State<BaseEnemy> stHurt;
    public State<BaseEnemy> stDeath;

    public Animator animator;

    [Header("AI Settings")]
    public float attackPreparationSeconds = 1.5f;
    public float recoveryAfterAttack = 2f;
    public float recoveryAfterHurt = 1f;
    public float attackRange = 2f;
    public float visionRange = 12f;

    public AttackArea attackArea;
    public DamageReceiver damageReceiver;

    public GameObject damageParticles;
    public GameObject deathParticles;
    public Vector3 particleOffset;

    void InstantiateParticles(GameObject prefab)
    {
        GameObject go = Instantiate(prefab);
        go.transform.position = transform.position + particleOffset;
    }

    internal void OnDeathAnimationFinished()
    {
        InstantiateParticles(deathParticles);
        transform.DOScale(Vector3.one * 0.2f, 0.3f).SetEase(Ease.InCubic).OnComplete(()=>Destroy(gameObject,0.1f));
    }


    private float recoveryCounter = 0;
    private float attackPreparationCounter = 0;
    private NavMeshAgent mAgent;

    private Transform target;
    public bool aiActive;

    private void Awake()
    {
        aiActive = false;
        mAgent = GetComponent<NavMeshAgent>();
        damageReceiver.OnDamageReceived += DamageReceiver_OnDamageReceived;
        damageReceiver.OnDeath += DamageReceiver_OnDeath;
    }

    private void DamageReceiver_OnDeath(DamageReceiver arg1, bool arg2)
    {
        fsm.ChangeState(stDeath);
        AudioController.Instance.PlaySFXSkeletonDeath();
    }

    private void DamageReceiver_OnDamageReceived(DamageReceiver arg1, bool arg2)
    {
        InstantiateParticles(damageParticles);

        mAgent.Move(-ToTarget().normalized*0.25f);
        if (fsm.IsInState(stIdle))
        {
            fsm.ChangeState(stHurt);
        }
    }

    void Start()
    {
        Invoke("DelayedActivation", 0.1f);
    }

    void DelayedActivation()
    {
            InitAI();
    }

    void InitAI()
    {
        stIdle = new BaseEnemy_StIdle();
        stChase = new BaseEnemy_StChase();
        stAttack = new BaseEnemy_StAttack();
        stHurt = new BaseEnemy_StHurt();
        stDeath = new BaseEnemy_StDeath();
        stReposition = new BaseEnemy_StReposition();
        fsm = new StateMachine<BaseEnemy>(this, stIdle, null);
        target = Gamemanager.Instance.GetPlayer().transform;
        aiActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (aiActive)
        { 
            fsm.DoUpdate();
        }
    }

    public bool CanReachPlayer()
    {
        NavMeshPath path = new NavMeshPath();
        mAgent.CalculatePath(target.position, path);
        return path.status == NavMeshPathStatus.PathComplete;
    }

    public bool CanAttackPlayer()
    {
        return DistanceToTarget() < attackRange && Mathf.Abs(AngleToTarget()) < 45;
    }

    public bool CanSeePlayer()
    {
        return DistanceToTarget() < visionRange;
    }

    private float AngleToTarget()
    {
        Vector3 toTarget = target.position - transform.position;
        Vector3 forward = transform.forward;
        return Vector3.Angle(toTarget, forward);

    }

    private Vector3 ToTarget()
    {
        return target.position - transform.position;
    }

    private float DistanceToTarget()
    {
        return Vector3.Distance(transform.position, target.position);
    }

    public bool IsRecovering()
    {
        return recoveryCounter > 0;
    }

    public void TickRecovery()
    {        
        recoveryCounter -= Time.deltaTime;
    }
    public void SetRecovery()
    {
        recoveryCounter = recoveryAfterAttack;
    }

    float chaseSpeed = 3;
    /// <summary>
    /// Get closer to player using navmeshagent
    /// </summary>
    public void ChasePlayer()
    {
        mAgent.isStopped = false;
        mAgent.speed = chaseSpeed;
        Vector3 offset = (target.position - transform.position).normalized;
        mAgent.SetDestination(target.position-offset*attackRange*0.75f);        
    }

    public void StopMovement()
    {
        mAgent.isStopped = true;
    }

    
    public bool IsPreparingAttack()
    {
        return attackPreparationCounter > 0;
    }

    public void TickAttackPreparation()
    {
        attackPreparationCounter -= Time.deltaTime;
    }


    public void ResetAttackPreparation()
    {
        attackPreparationCounter = attackPreparationSeconds;
    }

    public void AttackRoutine()
    {
        float attackHitDelay = 0.7f;
        Vector3 punchVector = transform.forward *attackRange*0.2f;
        StartCoroutine(AttackBlockCoroutine(1.75f));
        transform.DOPunchPosition(punchVector, 0.35f, 2).SetDelay(attackHitDelay).OnComplete(()=> attackArea.Attack(gameObject, 0.01f));        
    }

    public bool isCurrentlyAttacking = false;
    IEnumerator AttackBlockCoroutine(float blockSeconds)
    {
        isCurrentlyAttacking = true;
        yield return new WaitForSeconds(blockSeconds);
        isCurrentlyAttacking = false;
    }

    public void SetHurtRecovery()
    {
        recoveryCounter = recoveryAfterHurt;
    }



    Vector3 repositionPoint;
    internal void GetRepositionPoint()
    {
        repositionPoint = transform.position + target.forward*Random.Range(0.25f, 1.5f) + target.right * Random.Range(-attackRange , attackRange);
        
    }

    internal bool ReachedReposition()
    {
        return Vector3.Distance(transform.position, repositionPoint) < 0.1f;
    }

    internal bool CanReachRepositionPoint()
    {
        NavMeshPath path = new NavMeshPath();
        mAgent.CalculatePath(repositionPoint, path);
        return path.status == NavMeshPathStatus.PathComplete;
    }

    public float repositionSpeed = 5;
    internal void MoveToReposition()
    {
        mAgent.isStopped = false;
        mAgent.speed = repositionSpeed;
        mAgent.SetDestination(repositionPoint);
    }


    #region animation

    public void AttackAnimation()
    {
        Debug.Log(string.Format("The entity {0} is PLAYING ANIMATION: {1}", gameObject.name, "Attack"));
        animator.SetBool("HOLD", false);
    }

    public void AttackPreparationAnimation()
    {
        Debug.Log(string.Format("The entity {0} is PLAYING ANIMATION: {1}", gameObject.name, "AttackPreparation"));
        animator.SetBool("HOLD", true);
        animator.SetTrigger("ATTACK");

    }

    public void HurtAnimation()
    {
        Debug.Log(string.Format("The entity {0} is PLAYING ANIMATION: {1}", gameObject.name, "Hurt"));
        animator.SetTrigger("HURT");

    }

    public void DeathAnimation()
    {
        Debug.Log(string.Format("The entity {0} is PLAYING ANIMATION: {1}", gameObject.name, "Death"));
        animator.SetTrigger("DEATH");

    }

    public void WalkAnimation()
    {
        Debug.Log(string.Format("The entity {0} is PLAYING ANIMATION: {1}", gameObject.name, "Walk"));
        animator.SetBool("WALK", true);
    }

    public void IdleAnimation()
    {
        Debug.Log(string.Format("The entity {0} is PLAYING ANIMATION: {1}", gameObject.name, "Idle"));
        animator.SetBool("WALK", false);
    }


    #endregion

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRange);

    }

    private void OnDrawGizmos()
    {
        if (fsm != null)
        {
            Gamemanager.drawString(fsm.CurrentState().ToString(), transform.position + Vector3.up * 2f);
            if (fsm.IsInState(stChase))
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, target.position);
            }
            else if(fsm.IsInState(stReposition))
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawLine(transform.position, repositionPoint);
                Gizmos.DrawWireSphere(repositionPoint, 0.25f);
            }
        }
    }

}


public class BaseEnemy_StIdle : State<BaseEnemy>
{
    public override void Enter(BaseEnemy owner)
    {
        owner.IdleAnimation();
    }

    public override void Execute(BaseEnemy owner)
    {
        if (owner.IsRecovering())
        {
            owner.TickRecovery();
            return;
        }

        if (owner.CanSeePlayer() && owner.CanReachPlayer())
        {
            owner.fsm.ChangeState(owner.stChase);
        }
    }

    public override void Exit(BaseEnemy owner)
    {
    }
}

public class BaseEnemy_StChase : State<BaseEnemy>
{
    public override void Enter(BaseEnemy owner)
    {
        owner.WalkAnimation();
    }

    public override void Execute(BaseEnemy owner)
    {        

        if (owner.CanAttackPlayer())
        {
            owner.fsm.ChangeState(owner.stAttack);
        }
        else if (!owner.CanReachPlayer())
        {
            owner.fsm.ChangeState(owner.stIdle);
        }
        else
        {
            owner.ChasePlayer();
        }
    }

    public override void Exit(BaseEnemy owner)
    {
        owner.StopMovement();
    }
}


public class BaseEnemy_StReposition : State<BaseEnemy>
{
    public override void Enter(BaseEnemy owner)
    {
        owner.WalkAnimation();
        owner.GetRepositionPoint();
    }

    public override void Execute(BaseEnemy owner)
    {
        if (owner.isCurrentlyAttacking)
            return;

        if (owner.ReachedReposition())
        {
            owner.fsm.ChangeState(owner.stChase);
        }
        else if (!owner.CanReachRepositionPoint())
        {
            owner.fsm.ChangeState(owner.stChase);
        }
        else
        {
            owner.MoveToReposition();
        }
    }

    public override void Exit(BaseEnemy owner)
    {
        owner.IdleAnimation();
        owner.StopMovement();
    }
}

public class BaseEnemy_StAttack : State<BaseEnemy>
{
    public override void Enter(BaseEnemy owner)
    {
        owner.ResetAttackPreparation();
        owner.AttackPreparationAnimation();
    }

    public override void Execute(BaseEnemy owner)
    {
        if (owner.IsPreparingAttack())
        {
            owner.TickAttackPreparation();
        }
        else
        {
            owner.AttackAnimation();
            owner.AttackRoutine();
            //Move inside attack routine
            //->
            float dice = Random.Range(0f, 1f);
            if (dice < 0.1f)
            {
                owner.SetRecovery();
                owner.fsm.ChangeState(owner.stIdle);
            }
            else
            {
                owner.SetRecovery();
                owner.fsm.ChangeState(owner.stReposition);
            }
            //^^
        }
    }

    public override void Exit(BaseEnemy owner)
    {
    }
}

public class BaseEnemy_StHurt : State<BaseEnemy>
{
    public override void Enter(BaseEnemy owner)
    {
        owner.HurtAnimation();
        owner.SetHurtRecovery();
    }

    public override void Execute(BaseEnemy owner)
    {
        if (owner.IsRecovering())
        {
            owner.TickRecovery();
        }
        else
        {
            owner.fsm.ChangeState(owner.stIdle);
        }
    }

    public override void Exit(BaseEnemy owner)
    {
    }
}

public class BaseEnemy_StDeath : State<BaseEnemy>
{
    public override void Enter(BaseEnemy owner)
    {
        owner.DeathAnimation();
    }

    public override void Execute(BaseEnemy owner)
    {
    }

    public override void Exit(BaseEnemy owner)
    {
    }
}
