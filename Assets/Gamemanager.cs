﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gamemanager : MonoBehaviour
{
    public GameObject playerPrefab;
    public Transform spawnPoint;

    public static Gamemanager Instance;
    private CheckPoint lastCheckpoint;

    private GameObject player;
    public string musicName;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        
        SpawnPlayer(spawnPoint);
        CameraManager.Instance.Setup(player);
        StartGame();
        Invoke("DelayMusicTransition", 0.1f);
    }

    void DelayMusicTransition()
    {
        AudioController.Instance.MusicTransition(musicName);
    }

    public void HealthRecovery(int hp)
    {
        var dr= player.GetComponentInChildren<DamageReceiver>();
        dr.Health++;
        LifeUIManager.Instance.SetHP((int)dr.Health);
        AudioController.Instance.PlaySFXLifeRecover();


    }
    public GameObject GetPlayer()
    {
        return player;
    }

    void SpawnPlayer(Transform spawnPoint)
    {
        player = Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation, null);
    }

    public void SetCheckpoint(CheckPoint checkpoint)
    {
        lastCheckpoint = checkpoint;
    }


    public CheckPoint GetLastCheckpoint()
    {
        return lastCheckpoint;
    }

    void StartGame()
    {
        int maxHP = (int)player.GetComponentInChildren<DamageReceiver>().maxHealth;
        LifeUIManager.Instance.Init(maxHP);
        LifeUIManager.Instance.SetHP(maxHP);
        var dr = player.GetComponentInChildren<DamageReceiver>();
        dr.OnDamageReceived += Dr_OnDamageReceived;
        dr.OnDeath += Dr_OnDeath;
    }

    private void Dr_OnDeath(DamageReceiver arg1, bool arg2)
    {
        SceneManager.LoadScene("GameOverScene");
    }

    private void Dr_OnDamageReceived(DamageReceiver obj, bool lethalDamage)
    {
        PostProcessManager.Instance.DamageEffect();
        CameraManager.Instance.ScreenShake();
        Debug.Log("Player received damage!");
        if (lethalDamage)
        {
            RespawnInCheckpoint();
            AudioController.Instance.PlaySFXSpikeDamage();

        }
        else
        {
            AudioController.Instance.PlaySFXCombatDamage();
        }
        LifeUIManager.Instance.LoseHP((int)obj.Health);
    }


    void RespawnInCheckpoint()
    {
        var pm = player.GetComponent<PlayerMovement>();
        pm.Stop();
        CommandManager.Instance.ClearAllCommands();
        CommandManager.Instance.StopCurrentCommand();

        if (lastCheckpoint)
        {
            pm.Teleport(lastCheckpoint.transform);
        }
        else
        {
            pm.Teleport(Gamemanager.Instance.transform);
        }
        AudioController.Instance.PlaySFXSpawnCheckPoint();
    }

    static public void drawString(string text, Vector3 worldPos, float oX = 0, float oY = 0, Color? colour = null)
    {

        #if UNITY_EDITOR
        UnityEditor.Handles.BeginGUI();

        var restoreColor = GUI.color;

        if (colour.HasValue) GUI.color = colour.Value;
        var view = UnityEditor.SceneView.currentDrawingSceneView;
        Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);

        if (screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.z < 0)
        {
            GUI.color = restoreColor;
            UnityEditor.Handles.EndGUI();
            return;
        }

        UnityEditor.Handles.Label(TransformByPixel(worldPos, oX, oY), text);

        GUI.color = restoreColor;
        UnityEditor.Handles.EndGUI();
        #endif
    }

    static Vector3 TransformByPixel(Vector3 position, float x, float y)
    {
        return TransformByPixel(position, new Vector3(x, y));
    }

    static Vector3 TransformByPixel(Vector3 position, Vector3 translateBy)
    {
        Camera cam = UnityEditor.SceneView.currentDrawingSceneView.camera;
        if (cam)
            return cam.ScreenToWorldPoint(cam.WorldToScreenPoint(position) + translateBy);
        else
            return position;
    }
}
