﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StopUIManager : MonoBehaviour
{
    public static StopUIManager Instance;
    public Image fillImage;

    private void Awake()
    {
        Instance = this;
    }

    public void SetNormalizedValue(float n)
    {
        fillImage.fillAmount = n;
    }
}
