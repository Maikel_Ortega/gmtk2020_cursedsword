﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    Transform targetTransform;
    void Start()
    {
        targetTransform = CameraManager.Instance.GetComponent<Cinemachine.CinemachineVirtualCamera>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = targetTransform.rotation;
        transform.Rotate(0, 180, 0);
    }
}
