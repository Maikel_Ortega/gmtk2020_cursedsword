﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    int recoveryAmount = 1;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Gamemanager.Instance.HealthRecovery(recoveryAmount);
            Destroy(gameObject);
            //AUDIO
            //PARTICLES
        }
    }
}
