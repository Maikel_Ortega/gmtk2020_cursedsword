﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public List<Vector3> points;
    public float speed;
    public float secondsToRest = 0.5f;
    public float initialDelay = 0f;

    private List<Vector3> _internalPoints;
    private Vector3 velocity;

    private int currentIndex = 0;
    private Vector3 nextPoint;

    private bool resting = true;
    private float restCounter = 0f;

    void Start()
    {
        transform.position = transform.TransformPoint(points[0]);
        _internalPoints = new List<Vector3>();
        if (initialDelay > 0f)
            resting = true;
        restCounter += initialDelay;
        for(int i = 0; i < points.Count; i++)
        {
            _internalPoints.Add(transform.TransformPoint(points[i]));
        }
        GetNextPoint();
    }

    void GetNextPoint()
    {
        nextPoint = _internalPoints[(currentIndex + 1) % points.Count];
    }

    private void Update()
    {
        if (resting)
        {
            velocity = Vector3.zero;
            restCounter -= Time.deltaTime;
            if (restCounter < 0)
            {
                resting = false;
            }
        }
        else
        {
            if (Vector3.Distance(transform.position, nextPoint) < 0.05f)
            {
                currentIndex++;
                currentIndex = currentIndex % points.Count;
                GetNextPoint();
                resting = true;
                restCounter = secondsToRest;
            }
            else
            { 
                velocity = (nextPoint - transform.position).normalized * speed;
                transform.Translate(velocity * Time.deltaTime,Space.World);
            }
        }
    }


    public Vector3 GetVelocity()
    {
        return velocity;
    }

    private void OnDrawGizmos()
    {
        if (points.Count == 0)
            return;
        for (int i = 0; i < points.Count; i++)
        {
            Gizmos.color = Color.cyan;
            if (Application.isPlaying)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawWireSphere(_internalPoints[i], 0.1f);
                Gizmos.DrawLine(_internalPoints[i], _internalPoints[(i + 1) % points.Count]);
            }
            else
            {             
                Gizmos.DrawWireSphere(transform.TransformPoint(points[i]), 0.1f);
                Gizmos.DrawLine(transform.TransformPoint(points[i]), transform.TransformPoint(points[(i + 1) % points.Count]));
            }
        }
    }
}
