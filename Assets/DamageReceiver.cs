﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DamageReceiver : MonoBehaviour
{
    public float maxHealth = 5;
    public float forceMultiplier = 3f;
    public bool useTween = false;
    public bool blockDamage = false;
    private float health;
    public float Health { get => health; set => health = Mathf.Clamp(value,0,maxHealth); }

    public bool isRecovering = false;
    public float damageRecoverySeconds = 0.5f;
    public Transform ownerTransform;
    public event System.Action<DamageReceiver,bool> OnDamageReceived;
    public event System.Action<DamageReceiver, bool> OnDeath;
    public bool destroyOnDeath = false;
    

    public GameObject damageParticles;
    public GameObject blockParticles;
    public GameObject deathParticles;
    public Vector3 particleOffset;

    public GameObject dropItemPrefab;

    [Range(0, 1)]
    public float dropProbability;

    public enum damageableTypes {SKELETON, PLAYER, BARREL}
    public damageableTypes itemType;


    GameObject InstantiateParticles(GameObject prefab)
    {
        GameObject go = Instantiate(prefab);
        go.transform.position = transform.position + particleOffset;
        return go;
    }

    private void Awake()
    {
        Health = maxHealth;
    }

    void BlockFeedback(GameObject damager)
    {
        var parts = InstantiateParticles(blockParticles);
        parts.transform.position += transform.forward;
        AudioController.Instance.PlaySFXUIForward();
    }

    public void DealDamage(GameObject owner, float attackDamage, bool lethalDamage = false)
    {
        if (isRecovering)
            return;

        Debug.Log(ownerTransform.gameObject + " received an attack!");
        if(lethalDamage)
            Debug.Log("Is LETHAL");


        if (blockDamage)
        { 
            Debug.Log("DAMAGE BLOCKED!");
            BlockFeedback(owner);
            return;
        }

        Health -= attackDamage;
        if (Health == 0)
        {
            Death();
        }
        else
        {
            if (OnDamageReceived != null)
            {
                OnDamageReceived(this, lethalDamage);
            }
            DamageReaction(owner);
            ActivateDamageRecovery();
        }
    }

    void DropItem()
    {
        Vector3 dir = transform.position - Gamemanager.Instance.GetPlayer().transform.position;
        dir = dir.normalized;
        Instantiate(dropItemPrefab, transform.position  + Vector3.up*0.5f, Quaternion.identity);

    }

    void Death()
    {
        if (OnDeath!= null)
        {
            OnDeath(this, false);
        }

        switch (itemType)
        {
            case damageableTypes.SKELETON:
                break;
            case damageableTypes.PLAYER:
                AudioController.Instance.PlaySFXCrushedDeath();
                break;
            case damageableTypes.BARREL:
                AudioController.Instance.PlaySFXDestroyBarrel();                
                break;
            default:
                break;
        }

        if (damageParticles != null)
        {
            InstantiateParticles(deathParticles);
        }

        if (ownerTransform != null && destroyOnDeath)
        {
            if (dropItemPrefab != null)
            {
                float dice = UnityEngine.Random.Range(0f, 1f);
                if (dice < dropProbability)
                {
                    DropItem();
                }
            }


            Destroy(ownerTransform.gameObject);
            //Animacion de muerte
            //Tween
            //Particulas
            //FX
        }

    }

    void DamageReaction(GameObject attacker)
    {
        if (damageParticles != null)
        {
            InstantiateParticles(damageParticles);
        }

        if (ownerTransform != null && useTween)
        {
            Vector3 punch = new Vector3(-0.2f, 0.4f, -0.2f);
            ownerTransform.DOPunchScale(punch, 0.2f, 5);
        }
        Rigidbody r = GetComponent<Rigidbody>();
        if (r != null)
        {
            Vector3 forceVector = transform.position - attacker.transform.position;
            forceVector =  Vector3.up*2;
            r.AddForce(forceVector.normalized*forceMultiplier, ForceMode.Impulse);
        }
    }

    public void ActivateDamageRecovery()
    {
        StartCoroutine(RecoveryCoroutine());
        //blink        
    }

    IEnumerator RecoveryCoroutine()
    {
        isRecovering = true;
        yield return new WaitForSeconds(damageRecoverySeconds);
        isRecovering = false;
    }
}
