﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommandManagerUI : MonoBehaviour
{
    public GameObject actionBubblePrefab;
    public Dictionary<ActionCommand, ActionTextBubble> bubblesPerCommand;
    public static CommandManagerUI Instance;
    private void Awake()
    {
        bubblesPerCommand = new Dictionary<ActionCommand, ActionTextBubble>();
        Instance = this; 
    }

    public ActionTextBubble AddActionCommand(ActionCommand command)
    {
        GameObject newBubble = Instantiate(actionBubblePrefab, transform);
        var atb = newBubble.GetComponent<ActionTextBubble>();
        atb.Init(command.GetUiText());
        bubblesPerCommand.Add(command, atb);
        newBubble.transform.SetAsLastSibling();
        atb.CreationTween();        
        LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
        return atb;
    }

    public void ClearCommandBubble(ActionCommand command, bool immediate=false)
    {
        ActionTextBubble atb;
        if (bubblesPerCommand.TryGetValue(command, out atb))
        {
            bubblesPerCommand.Remove(command);
            atb.Kill(immediate);
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
    }


    // Update is called once per frame
    void Update()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());

    }
}
