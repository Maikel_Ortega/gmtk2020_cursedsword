﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var aa= GetComponent<AttackArea>();
        aa.Attack(gameObject, 0.05f);
    }
}
