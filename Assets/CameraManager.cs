﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class CameraManager : MonoBehaviour
{
    public AnimationCurve inCurve;
    public AnimationCurve outCurve;
    CinemachineVirtualCamera cm;
    public static CameraManager Instance;
    private void Awake()
    {
        Instance = this;
        cm = GetComponent<CinemachineVirtualCamera>();
    }

    public void ScreenShake()
    {
        StopAllCoroutines();
        StartCoroutine(ScreenShakeCoroutine(cm,0.2f,0.3f));
    }

    public void Setup(GameObject player)
    {
        cm.Follow = player.transform;
        cm.LookAt = player.transform.Find("ForwardGizmo");
    }

    IEnumerator ScreenShakeCoroutine(CinemachineVirtualCamera vc, float secondsToMax, float secondsToReturn)
    {
        float counter = 0;
        float minAmp = 0;
        float maxAmp = 1.5f;
        float minFreq = 0;
        float maxFreq = 0.2f;
        var noise = vc.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

        while (counter < secondsToMax)
        {
            float normalizedCounter = counter / secondsToMax;
            float evaluatedValue = inCurve.Evaluate(normalizedCounter);
            noise.m_AmplitudeGain = Mathf.Lerp(minAmp, maxAmp, evaluatedValue);
            noise.m_FrequencyGain = Mathf.Lerp(minFreq, maxFreq, evaluatedValue);
            yield return null;
            counter += Time.deltaTime;
        }

        counter = 0;

        while (counter < secondsToReturn)
        {
            float normalizedCounter = counter / secondsToReturn;
            float evaluatedValue = outCurve.Evaluate(normalizedCounter);
            noise.m_AmplitudeGain = Mathf.Lerp(maxAmp, minAmp, evaluatedValue);
            noise.m_AmplitudeGain = Mathf.Lerp(maxFreq, minFreq, evaluatedValue);
            yield return null;
            counter += Time.deltaTime;
        }

    }
}
