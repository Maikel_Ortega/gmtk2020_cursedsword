﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;

public class SplashSceneController : MonoBehaviour
{
    public CanvasGroup cg;
    public RectTransform textRectTransform;
    private bool inputActive = false;
    public string musicName;

    private void Awake()
    {
        cg.alpha = 0;
        Sequence s = DOTween.Sequence();
        s.Append(cg.DOFade(1, 1.5f).SetEase(Ease.InCubic)).OnComplete
            (
                () =>
                {
                    inputActive = true;
                    cg.DOFade(0.25f, 1f).SetEase(Ease.InOutCubic).SetLoops(-1, LoopType.Yoyo).SetDelay(0.5f);

                }
            );        
        s.Play();
    }

    void Start()
    {
        if(musicName != "")
            AudioController.Instance.MusicTransition(musicName);
    }


    void Update()
    {
        if (inputActive)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                inputActive = false;
                cg.DOKill();
                cg.alpha = 1;
                textRectTransform.DOPunchScale(Vector3.one, 0.3f, 2).OnComplete(() => LoadNextLevel());
            }
        }
    }
    public string nextLevel;
    void LoadNextLevel()
    {
        SceneManager.LoadScene(nextLevel);
    }
}
