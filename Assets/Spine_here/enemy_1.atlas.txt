
enemy_1.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Knee_left
  rotate: true
  xy: 4, 15
  size: 52, 90
  orig: 54, 91
  offset: 1, 1
  index: -1
arm_1_left
  rotate: false
  xy: 359, 626
  size: 58, 67
  orig: 60, 70
  offset: 1, 1
  index: -1
arm_left
  rotate: false
  xy: 222, 166
  size: 100, 135
  orig: 102, 136
  offset: 1, 1
  index: -1
arm_right
  rotate: false
  xy: 106, 127
  size: 112, 146
  orig: 114, 148
  offset: 1, 1
  index: -1
body
  rotate: true
  xy: 436, 779
  size: 241, 279
  orig: 243, 281
  offset: 1, 1
  index: -1
cloth
  rotate: false
  xy: 719, 824
  size: 136, 196
  orig: 136, 196
  offset: 0, 0
  index: -1
elbow
  rotate: true
  xy: 421, 695
  size: 80, 81
  orig: 82, 83
  offset: 1, 1
  index: -1
eye
  rotate: true
  xy: 859, 826
  size: 39, 52
  orig: 39, 52
  offset: 0, 0
  index: -1
eye_1
  rotate: true
  xy: 986, 945
  size: 75, 34
  orig: 75, 34
  offset: 0, 0
  index: -1
femur_left
  rotate: false
  xy: 106, 4
  size: 110, 119
  orig: 112, 120
  offset: 1, 1
  index: -1
femur_right
  rotate: false
  xy: 220, 305
  size: 100, 139
  orig: 102, 141
  offset: 1, 1
  index: -1
head
  rotate: false
  xy: 859, 869
  size: 123, 151
  orig: 123, 151
  offset: 0, 0
  index: -1
knee_rigth
  rotate: false
  xy: 359, 697
  size: 58, 80
  orig: 60, 82
  offset: 1, 1
  index: -1
leg_left
  rotate: false
  xy: 247, 568
  size: 108, 209
  orig: 127, 224
  offset: 1, 15
  index: -1
leg_right
  rotate: false
  xy: 106, 277
  size: 110, 179
  orig: 110, 179
  offset: 0, 0
  index: -1
shoulder_left
  rotate: false
  xy: 247, 448
  size: 117, 116
  orig: 119, 118
  offset: 1, 1
  index: -1
shoulder_right
  rotate: true
  xy: 719, 718
  size: 102, 103
  orig: 106, 108
  offset: 1, 4
  index: -1
weapon
  rotate: false
  xy: 4, 71
  size: 98, 385
  orig: 100, 387
  offset: 1, 1
  index: -1
weapon_1
  rotate: false
  xy: 4, 781
  size: 428, 239
  orig: 428, 255
  offset: 0, 0
  index: -1
weapon_2
  rotate: false
  xy: 4, 460
  size: 239, 317
  orig: 247, 387
  offset: 1, 1
  index: -1
