
intro.png
size: 1024,256
format: RGBA8888
filter: Linear,Linear
repeat: none
background
  rotate: true
  xy: 4, 33
  size: 105, 327
  orig: 107, 327
  offset: 1, 0
  index: -1
background_1
  rotate: true
  xy: 337, 178
  size: 74, 327
  orig: 76, 327
  offset: 1, 0
  index: -1
face
  rotate: false
  xy: 4, 4
  size: 37, 25
  orig: 39, 27
  offset: 1, 1
  index: -1
line
  rotate: true
  xy: 4, 142
  size: 110, 329
  orig: 112, 331
  offset: 1, 1
  index: -1
wizard_1
  rotate: true
  xy: 668, 101
  size: 151, 278
  orig: 153, 279
  offset: 1, 1
  index: -1
wizard_2
  rotate: true
  xy: 337, 23
  size: 151, 278
  orig: 153, 279
  offset: 1, 1
  index: -1
