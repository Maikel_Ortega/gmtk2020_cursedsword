﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAnimatorInyector : MonoBehaviour
{
    public enum SWORD_MOOD { NORMAL, ANGRY, I_HOPE_SENPAI_NOTICES_ME_DESUDESU}
    public enum EYE_TYPE{ NORMAL, LEFT, RIGHT, UP, DOWN, STOP }
    private SWORD_MOOD currentMood;

    public Animator eyeAnimator;
    public Animator mouthAnimator;
    public Animator swordAnimator;

    public void SetMood(SWORD_MOOD m)
    {
        m = currentMood;
        switch (m)
        {
            case SWORD_MOOD.NORMAL:
                break;
            case SWORD_MOOD.ANGRY:
                break;
            case SWORD_MOOD.I_HOPE_SENPAI_NOTICES_ME_DESUDESU:
                break;
        }
    }

    /// <summary>
    /// Choose between SHORT, LONG y DOUBLE
    /// </summary>
    /// <param name=""></param>
    public void PlayMouthAnimation(string animation)
    {
        mouthAnimator.Play(animation);
    }

    public void ChangeEye(EYE_TYPE eye)
    { 

    }
}