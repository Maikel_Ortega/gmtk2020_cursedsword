﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour
{
    [Header("MIXER")]
    [Space()]
    public AudioMixer mixer;
    [Header("CHANNELS")]
    [Space()]
    public AudioMixerGroup musicGroup;
    public AudioMixerGroup voGroup;
    public AudioMixerGroup sfxGroup;
    public AudioMixerGroup uiGroup;
    public AudioMixerGroup footstepsGroup;

    [Header("SNAPSHOTS")]
    [Space()]
    public AudioMixerSnapshot menuSnapshot; 
    public AudioMixerSnapshot gameplaySnapshot;
    public AudioMixerSnapshot gameoverSnapshot;

    [Header("MUSIC")]
    [Space()]
    public AudioMixerGroup gameplayMusicGroup;
    public AudioSource gameplayMusicAudioSource; 
    public AudioClip gameplayMusicAudioClip;
    [Space()]
    public AudioMixerGroup menuMusicGroup;
    public AudioSource menuMusicAudioSource;
    public AudioClip menuMusicAudioClip;
    [Space()]
    public AudioMixerGroup gameoverMusicGroup;
    public AudioSource gameoverMusicAudioSource;
    public AudioClip gameoverMusicAudioClip;
    [Space()]
    public float fadeTime;
    string musicToBeStopped;

    [Header("VO")]
    [Space()]
    public AudioSource voAudioSource;
    
    [Space()]
    public AudioClip[] voLeftClips;
    [Space()]
    public AudioClip[] voRightClips;
    [Space()]
    public AudioClip[] voAccelerateClips;
    [Space()]
    public AudioClip[] voBrakeClips;
    [Space()]
    public AudioClip[] voStopClips;
    [Space()]
    public AudioClip[] voDeathClips;
    [Space()]
    public AudioClip[] voCelebration;
    public AudioClip[] voAttackClips;

    [Header("FOOTSTEPS")]
    [Space()]
    public AudioSource footstepsAudioSource;
    [Space()]
    public AudioClip[] footstepsAudioClips;
    [Space()]
    public AudioClip[] fastfootstepsAudioClips;
    [Space()]

    [Header("SFX")]
    [Space()]
    public AudioSource spikeDamageAudioSource;
    public AudioClip spikeDamageAudioClip;
    public AudioSource crushedDeathAudioSource;
    public AudioClip crushedDeathAudioClip;
    public AudioSource combatDamageAudioSource;
    public AudioClip combatDamageAudioClip;
    public AudioSource lifeRecoverAudioSource;
    public AudioClip lifeRecoverAudioClip;
    public AudioSource checkpointAudioSource;
    public AudioClip checkpointAudioClip;
    public AudioSource spawnCheckpointAudioSource;
    public AudioClip spawnCheckpointAudioClip;
    public AudioSource endLevelAudioSource;
    public AudioClip endLevelAudioClip;
    public AudioSource movingPlatformAudioSource;
    public AudioClip movingPlatformAudioClip;
    public AudioSource stopMovingPlatformAudioSource;
    public AudioClip stopMovingPlatformAudioClip;
    public AudioSource destroyBarrelAudioSource;
    public AudioClip destroyBarrelAudioClip;
    public AudioSource lifeItemAppearsAudioSource;
    public AudioClip lifeItemAppearsAudioClip;
    public AudioSource skeletonAttacksAudioSource;
    public AudioClip skeletonAttacksAudioClip;
    public AudioSource skeletonDeathAudioSource;
    public AudioClip skeletonDeathAudioClip;
    public AudioSource sleketonIdleAudioSource;
    public AudioClip sleketonIdleAudioClip;

    [Header("UI")]
    [Space()]
    public AudioSource UIAudioSource;
    [Space()]
    public AudioClip ButtonForwardAudioClip;
    public AudioClip ButtonBackAudioClip;

    int lastLeftPlayed = 0;
    int lastRightPlayed = 0;
    int lastAcceleratePlayed = 0;
    int lastBrakePlayed = 0;
    int lastStopPlayed = 0;
    int lastCelebrationPlayed = 0;
    int lastDeathPlayed = 0;
    int lastFootStep = 0;

    int lastAttackPlayed = 0;

    public static AudioController Instance;

    private void Awake()
    {
        if (Instance != null)
        { 
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        musicToBeStopped = "";
        gameplayMusicAudioSource= gameObject.AddComponent<AudioSource>();
        menuMusicAudioSource= gameObject.AddComponent<AudioSource>();
        gameoverMusicAudioSource= gameObject.AddComponent<AudioSource>();
        voAudioSource= gameObject.AddComponent<AudioSource>();
        footstepsAudioSource= gameObject.AddComponent<AudioSource>();
        spikeDamageAudioSource= gameObject.AddComponent<AudioSource>();
        crushedDeathAudioSource= gameObject.AddComponent<AudioSource>();
        combatDamageAudioSource= gameObject.AddComponent<AudioSource>();
        lifeRecoverAudioSource= gameObject.AddComponent<AudioSource>();
        checkpointAudioSource= gameObject.AddComponent<AudioSource>();
        spawnCheckpointAudioSource= gameObject.AddComponent<AudioSource>();
        endLevelAudioSource= gameObject.AddComponent<AudioSource>();
        movingPlatformAudioSource= gameObject.AddComponent<AudioSource>();
        stopMovingPlatformAudioSource= gameObject.AddComponent<AudioSource>();
        destroyBarrelAudioSource= gameObject.AddComponent<AudioSource>();
        lifeItemAppearsAudioSource= gameObject.AddComponent<AudioSource>();
        skeletonAttacksAudioSource= gameObject.AddComponent<AudioSource>();
        skeletonDeathAudioSource= gameObject.AddComponent<AudioSource>();
        sleketonIdleAudioSource= gameObject.AddComponent<AudioSource>();
        UIAudioSource= gameObject.AddComponent<AudioSource>();

        gameplayMusicAudioSource.clip = gameplayMusicAudioClip;
        gameplayMusicAudioSource.outputAudioMixerGroup = gameplayMusicGroup;
        menuMusicAudioSource.clip = menuMusicAudioClip;
        menuMusicAudioSource.outputAudioMixerGroup = menuMusicGroup;
        gameoverMusicAudioSource.clip = gameoverMusicAudioClip;
        gameoverMusicAudioSource.outputAudioMixerGroup = gameoverMusicGroup;

        voAudioSource.outputAudioMixerGroup = voGroup;
        footstepsAudioSource.outputAudioMixerGroup = footstepsGroup;
        spikeDamageAudioSource.outputAudioMixerGroup = sfxGroup;
        crushedDeathAudioSource.outputAudioMixerGroup = sfxGroup;
        combatDamageAudioSource.outputAudioMixerGroup = sfxGroup;
        lifeRecoverAudioSource.outputAudioMixerGroup = sfxGroup;
        checkpointAudioSource.outputAudioMixerGroup = sfxGroup;
        spawnCheckpointAudioSource.outputAudioMixerGroup = sfxGroup;
        endLevelAudioSource.outputAudioMixerGroup = sfxGroup;
        movingPlatformAudioSource.outputAudioMixerGroup = sfxGroup;
        stopMovingPlatformAudioSource.outputAudioMixerGroup = sfxGroup;
        destroyBarrelAudioSource.outputAudioMixerGroup = sfxGroup;
        lifeItemAppearsAudioSource.outputAudioMixerGroup = sfxGroup;
        skeletonAttacksAudioSource.outputAudioMixerGroup = sfxGroup;
        skeletonDeathAudioSource.outputAudioMixerGroup = sfxGroup;
        sleketonIdleAudioSource.outputAudioMixerGroup = sfxGroup;
        UIAudioSource.outputAudioMixerGroup = uiGroup;



        gameplayMusicAudioSource.loop = true;
        menuMusicAudioSource.loop = true;
        gameoverMusicAudioSource.loop = true;

        // vo have to be played manually
        // footsteps have to be played manually
        spikeDamageAudioSource.clip = spikeDamageAudioClip;
        crushedDeathAudioSource.clip = crushedDeathAudioClip;
        combatDamageAudioSource.clip = combatDamageAudioClip;
        lifeRecoverAudioSource.clip = lifeRecoverAudioClip;
        checkpointAudioSource.clip = checkpointAudioClip;
        spawnCheckpointAudioSource.clip = spawnCheckpointAudioClip;
        endLevelAudioSource.clip = endLevelAudioClip;
        movingPlatformAudioSource.clip = movingPlatformAudioClip;
        stopMovingPlatformAudioSource.clip = stopMovingPlatformAudioClip;
        destroyBarrelAudioSource.clip = destroyBarrelAudioClip;
        lifeItemAppearsAudioSource.clip = lifeItemAppearsAudioClip;
        skeletonAttacksAudioSource.clip = skeletonAttacksAudioClip;
        skeletonDeathAudioSource.clip = skeletonDeathAudioClip;
        sleketonIdleAudioSource.clip = sleketonIdleAudioClip;
        // UI have to be played manually       
    }

    public void MusicTransition(string scene){
    
        if (scene == "Intro") {
            
            menuMusicAudioSource.Play();
            menuSnapshot.TransitionTo(fadeTime);
            musicToBeStopped = "Gameplay";
            Invoke("MusicStop", fadeTime);
        }
        if (scene == "Gameplay"){
            
            gameplayMusicAudioSource.Play();
            gameplaySnapshot.TransitionTo(fadeTime);
            musicToBeStopped = "Menu";
            Invoke("MusicStop", fadeTime);
        }
        if (scene == "Gameover"){

            gameoverMusicAudioSource.Play();
            gameoverSnapshot.TransitionTo(fadeTime);
            musicToBeStopped = "Gameplay";
            Invoke("MusicStop", fadeTime); 
        }
    
    }

    public void MusicStop(){
        
        if (musicToBeStopped == "Gameplay") {
            gameplayMusicAudioSource.Stop();
            musicToBeStopped ="";
        } else if (musicToBeStopped == "Menu") {
            menuMusicAudioSource.Stop();
            musicToBeStopped ="";
        } else if (musicToBeStopped == "Gameover"){
            gameoverMusicAudioSource.Stop();
            musicToBeStopped ="";
        }
    }

    public void PlaySFXFootstep(){
        int index = Random.Range(0, footstepsAudioClips.Length);
        while(index == lastFootStep){
            index = Random.Range(0, footstepsAudioClips.Length);
        }
        footstepsAudioSource.clip = footstepsAudioClips[index];
        footstepsAudioSource.Play();
        lastFootStep = index;
    }
    
    public void PlaySFXSpikeDamage(){
        spikeDamageAudioSource.Play();
    }

    public void PlaySFXCrushedDeath(){
        crushedDeathAudioSource.Play();
    }        

    public void PlaySFXCombatDamage(){
        combatDamageAudioSource.Play();
    }      

    public void PlaySFXLifeRecover(){
        lifeRecoverAudioSource.Play();
    }
    
    public void PlaySFXCheckPoint(){
        checkpointAudioSource.Play();
    }

    public void PlaySFXSpawnCheckPoint(){
        spawnCheckpointAudioSource.Play();
    }

    public void PlaySFXEndLevel(){
        endLevelAudioSource.Play();
    }

    public void PlaySFXMovingPlatform(){
        movingPlatformAudioSource.Play();
    }
    
    public void PlaySFXStopMovingPlatform(){
        movingPlatformAudioSource.Stop();
        stopMovingPlatformAudioSource.Play();
    }
    public void PlaySFXDestroyBarrel(){
        destroyBarrelAudioSource.Play();
    }

    public void PlaySFXLifeItemAppears(){
        
        lifeItemAppearsAudioSource.Play();
    }

    public void PlaySFXSkeletonAttack(){
        skeletonAttacksAudioSource.Play();
    }

    public void PlaySFXSkeletonDeath(){
        skeletonDeathAudioSource.Play();
    }
    
    public void PlaySFXSkeletonIdle(){
        sleketonIdleAudioSource.Play();
    }
        
    public void PlaySFXUIForward(){
        UIAudioSource.clip = ButtonForwardAudioClip;
        UIAudioSource.Play();
    }

    public void PlaySFXUIBack(){
        UIAudioSource.clip = ButtonBackAudioClip;
        UIAudioSource.Play();
    }

    public void PlayVOAttack(){
    int index = Random.Range(0, voAttackClips.Length);    
        while(index == lastAttackPlayed){
            index = Random.Range(0, voAttackClips.Length);
        }
        voAudioSource.clip = voAttackClips[index];
        voAudioSource.Play();    
        lastAttackPlayed = index;
    }
    public void PlayVOLeft(){
    int index = Random.Range(0, voLeftClips.Length);    
        while(index == lastLeftPlayed){
            index = Random.Range(0, voLeftClips.Length);
        }
        voAudioSource.clip = voLeftClips[index];
        voAudioSource.Play();    
        lastLeftPlayed = index;
    }
    public void PlayVORight(){
    int index = Random.Range(0, voRightClips.Length);    
        while(index == lastRightPlayed){
            index = Random.Range(0, voRightClips.Length);
        }
        voAudioSource.clip = voRightClips[index];
        voAudioSource.Play();
        lastRightPlayed = index;
    }
    public void PlayVOAccelerate(){
        int index = Random.Range(0, voAccelerateClips.Length);
        while(index == lastAcceleratePlayed){
            index = Random.Range(0, voAccelerateClips.Length);
        }
        voAudioSource.clip = voAccelerateClips[index];
        voAudioSource.Play();
        lastAcceleratePlayed = index;
    }
    public void PlayVOBrake(){
        int index = Random.Range(0, voBrakeClips.Length);
        while(index == lastRightPlayed){
            index = Random.Range(0, voBrakeClips.Length);
        }
        voAudioSource.clip = voBrakeClips[index];
        voAudioSource.Play();
        lastBrakePlayed = index;
    }
    public void PlayVOStop(){
        int index = Random.Range(0, voStopClips.Length);
        while(index == lastStopPlayed){
            index = Random.Range(0, voStopClips.Length);
        }
        voAudioSource.clip = voStopClips[index];
        voAudioSource.Play();
        lastStopPlayed = index;
    }
    public void PlayVODeath(){
        int index = Random.Range(0, voDeathClips.Length);
        while(index == lastDeathPlayed){
            index = Random.Range(0, voDeathClips.Length);
        }
        voAudioSource.clip = voDeathClips[index];
        voAudioSource.Play();
        lastDeathPlayed = index;
    }
    public void PlayVOCelebration(){
        int index = Random.Range(0, voCelebration.Length);
        while(index == lastCelebrationPlayed){
        index = Random.Range(0, voCelebration.Length);
        }
        voAudioSource.clip = voCelebration[index];
        voAudioSource.Play();
        lastCelebrationPlayed = index;
    }
}