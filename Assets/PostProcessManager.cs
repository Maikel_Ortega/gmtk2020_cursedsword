﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessManager : MonoBehaviour
{
    public static PostProcessManager Instance;
    PostProcessVolume volume;
    public AnimationCurve vignetteCurve;
    private Vignette v;
    private ChromaticAberration ca;
    private void Awake()
    {
        Instance = this;
        volume = GetComponent<PostProcessVolume>();
        volume.profile.TryGetSettings<Vignette>(out v);
        volume.profile.TryGetSettings<ChromaticAberration>(out ca);
    }

    public void DamageEffect()
    {        
        if (v)
        {
            StartCoroutine(DamageCoroutine(v, 0.3f, 0.4f));
        }
    }

    IEnumerator DamageCoroutine(Vignette v, float secondsToMax, float secondsToReturn)
    {
        float counter = 0;
        float maxVignette = 0.55f;
        float minVignette = v.intensity;

        Color baseColor = v.color;
        Color maxColor = Color.red;

        while (counter < secondsToMax)
        {
            float normalizedCounter = counter / secondsToMax;
            float evaluatedValue = vignetteCurve.Evaluate(normalizedCounter);
            v.intensity.value = Mathf.Lerp(minVignette, maxVignette, evaluatedValue);
            v.color.value = Color.Lerp(baseColor, maxColor, evaluatedValue);
            yield return null;
            counter += Time.deltaTime;
        }

        counter = 0;

        while (counter < secondsToReturn)
        {
            float normalizedCounter = counter / secondsToReturn;
            float evaluatedValue = vignetteCurve.Evaluate(normalizedCounter);
            v.intensity.value = Mathf.Lerp(maxVignette, minVignette, evaluatedValue);
            v.color.value = Color.Lerp(maxColor,baseColor, evaluatedValue);
            yield return null;
            counter += Time.deltaTime;
        }

    }

    public void SlowDownEffect()
    {
        float durationToSlow = 0.6f;
        float durationToNormal = 0.8f;
        Sequence s = DOTween.Sequence();
        s.Append(DOTween.To(GetTimeScale, SetTimeScale, 0.1f, durationToSlow).SetEase(Ease.OutQuad));
        s.Append(DOTween.To(GetTimeScale, SetTimeScale, 1f, durationToNormal).SetEase(Ease.InQuad).SetDelay(0.3f));
        s.SetUpdate(true);
        s.Play();
    }

    public void StopFX()
    {
        ChromaticAberrationBurst();
    }

    public void ChromaticAberrationBurst()
    {
        float durationIn= 0.1f;
        float durationOut = 0.9f;
        Sequence s = DOTween.Sequence();
        s.Append(DOTween.To(GetCAIntensity, SetCAIntensity, 1f, durationIn).SetEase(Ease.OutQuad));
        s.Append(DOTween.To(GetCAIntensity, SetCAIntensity, 0f, durationOut).SetEase(Ease.InQuad)).SetDelay(0.4f);
        s.SetUpdate(true);
        s.Play();
    }


    private void SetCAIntensity(float t)
    {
        ca.intensity.value = t;
    }
        
    private float GetCAIntensity()
    {
        return ca.intensity;
    }

    private float GetTimeScale()
    {
        return Time.timeScale;
    }


    private void SetTimeScale(float t)
    {
        Time.timeScale = t;
    }

}
