﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackArea : MonoBehaviour
{
    public Collider attackCollider;
    private Collider[] contacts = new Collider[10];
    public float  attackDamage = 1;
    public bool attacking = false;
    public LayerMask attackLayerMask;
    public bool lethalDamage = false;

    public void Attack(GameObject owner, float duration)
    {
        contacts = new Collider[10];
        StopAllCoroutines();
        StartCoroutine(AttackCoroutine(owner, duration));
    }

    private IEnumerator AttackCoroutine(GameObject owner, float duration)
    {
        attacking = true;
        float counter = duration; 

        while (counter > 0)
        {

            Vector3 worldCenter = attackCollider.bounds.center;
            Vector3 worldHalfExtents = attackCollider.bounds.size * 0.5f;            

            if (Physics.OverlapBoxNonAlloc(worldCenter, worldHalfExtents, contacts, attackCollider.transform.rotation, attackLayerMask)> 0)
            {
                foreach (var item in contacts)
                {
                    if (!item)
                        continue;
                    DamageReceiver dr = item.GetComponent<DamageReceiver>();
                    if (dr)
                    {
                        //Debug.Log("Detected a damageReceiver. Trying to hurt them!");
                        dr.DealDamage(owner, attackDamage,lethalDamage);
                    }
                }
            }
            counter -= Time.deltaTime;
            yield return null;
        }
        attacking = false;
    }

    private void OnDrawGizmos()
    {
        if (!attackCollider)
            return;

        if (attacking)
        {
            Gizmos.color = Color.red;
        }
        else
        {
            Gizmos.color = Color.green;
        }
        Gizmos.matrix = transform.localToWorldMatrix;
        Vector3 boxPosition = transform.InverseTransformPoint(attackCollider.bounds.center);
        Vector3 size = attackCollider.bounds.size;
        Gizmos.DrawWireCube(boxPosition, Vector3.one);        
    }
}
