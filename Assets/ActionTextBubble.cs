﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class ActionTextBubble : MonoBehaviour
{
    public TextMeshProUGUI tmProText;
    public CanvasGroup canvasGroup;

    public void Init(string text)
    {
        this.tmProText.text = text;
        canvasGroup.alpha = 0f;
    }

    public void CreationTween()
    {
        RectTransform r = GetComponent<RectTransform>();
        r.transform.localScale = Vector3.one * 0.9f;
        Sequence s = DOTween.Sequence();

        r.anchoredPosition += Vector2.right * 100f;

        s.Append(r.DOScale(1.3f, 0.1f).SetEase(Ease.InCubic));
        s.Join(canvasGroup.DOFade(1f, 0.01f));
        s.Append(r.DOAnchorPosX(125, 0.2f).SetEase(Ease.InCubic));
        s.Append(r.DOScale(1.0f, 0.3f).SetEase(Ease.OutBack));
        s.SetDelay(0.2f);
        s.Play();
    }

    public void Kill(bool immediate = false)
    {
        if (immediate)
        {
            Destroy(gameObject);
        }
        else
        { 
            RectTransform r = GetComponent<RectTransform>();
            Sequence s = DOTween.Sequence();
            s.Append(r.DOScale(1.2f, 0.2f).SetEase(Ease.OutCubic));
            s.Append(r.DOScale(0.01f, 0.1f).SetEase(Ease.InCubic));
            s.OnComplete(() => Destroy(gameObject));
            s.Play();
        }
    }
}
