﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Floating : MonoBehaviour
{
    public float maxLocalY = 1f;
    public float duration = 1.5f;
    // Start is called before the first frame update
    void Start()
    {
        transform.DOLocalMoveY(maxLocalY, duration).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutQuad);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
