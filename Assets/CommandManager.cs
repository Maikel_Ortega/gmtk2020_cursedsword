﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandManager : MonoBehaviour
{
    public List<ActionCommand> actionQueue;

    public ActionCommand currentCommand;
    public static CommandManager Instance;
    public float cooldownBetweenCommands = 0.5f;
    private float currentCD = 0f;
    public int maxCommands = 6;

    private void Awake()
    {
        actionQueue = new List<ActionCommand>();
        Instance = this;
    }

    public void QueueCommand(ActionCommand newCommand)
    {
        if (actionQueue.Count > maxCommands)
        {
            ClearOldestCommand();
            Debug.Log("Too many commands! Deleting one");
        }
        //Comprobar si hay que borrar comandos, limites, etc...
        Debug.Log(string.Format("<color=green> -> Adding new command: {0}</color>", newCommand.ToString()));
        actionQueue.Add(newCommand);
        CommandManagerUI.Instance.AddActionCommand(newCommand);
    }

    public void ClearOldestCommand()
    {
        ActionCommand c = actionQueue[actionQueue.Count-1];
        CommandManagerUI.Instance.ClearCommandBubble(c,true);
        actionQueue.Remove(c);
    }

    public void ClearAllCommands(bool instant = false)
    {
        foreach (var item in actionQueue)
        {            
            CommandManagerUI.Instance.ClearCommandBubble(item,instant);
        }
        actionQueue.Clear();
    }

    void CheckAvailableCommands()
    {
        if (actionQueue.Count > 0)
        {
            currentCommand = actionQueue[0];
            actionQueue.Remove(currentCommand);
            currentCommand.OnCommandFinished += CurrentCommand_OnCommandFinished;
            Debug.Log(string.Format("<color=cyan> Starting command: {0}</color>", currentCommand.ToString()));
            currentCommand.StartCommand(this);
        }
    }

    public void StopCurrentCommand()
    {
        CommandManagerUI.Instance.ClearCommandBubble(currentCommand, true);
        currentCommand = null;
        StopAllCoroutines();
    }

    private void CurrentCommand_OnCommandFinished(ActionCommand obj)
    {
        obj.OnCommandFinished -= CurrentCommand_OnCommandFinished;
        CommandManagerUI.Instance.ClearCommandBubble(obj);
        //Actualizar UI
        Debug.Log(string.Format("<color=darkgreen> >>> Command: {0} FINISHED</color>", currentCommand.ToString()));
    }

    void Update()
    {
        if (currentCommand != null && currentCommand.IsRunning && currentCD > 0)
        {            
            currentCD = Mathf.Clamp(currentCD - Time.deltaTime, 0, Mathf.Infinity);
        }

        if ((currentCommand == null || !currentCommand.IsRunning) && currentCD == 0)
        {            
            CheckAvailableCommands();
        }
    }
}
