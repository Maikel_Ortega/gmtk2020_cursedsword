﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LifeUIManager : MonoBehaviour
{
    public GameObject heartPrefab;
    public List<GameObject> hearts;

    public static LifeUIManager Instance;

    public void Awake()
    {
        Instance = this;
    }

    public void Init(int maxHP)
    {
        for (int i = 0; i < maxHP; i++)
        {
            hearts.Add(Instantiate(heartPrefab, transform));
        }
    }

    public void SetHP(int hp)
    {
        for (int i = 0; i < hearts.Count; i++)
        {
            if (i < hp)
            {
                hearts[i].gameObject.SetActive(true);
                RectTransform rt = hearts[i].gameObject.GetComponent<RectTransform>();
                Vector3 scale = new Vector3(0.6f, 0.6f, 0.6f);
                rt.localScale = scale;
                rt.DOPunchScale(Vector3.one,0.3f,4);
            }
            else
            {
                hearts[i].gameObject.SetActive(false);
            }
        }
    }

    public void LoseHP(int currentHP)
    {
        RectTransform rt = hearts[currentHP].gameObject.GetComponent<RectTransform>();
        Vector3 scale = new Vector3(0.1f, 0.1f, 1f);
        rt.DOScale(scale, 0.3f).SetEase(Ease.InBack).OnComplete(()=> SetHP(currentHP));
        //METER TWEEN
        
    }
}
