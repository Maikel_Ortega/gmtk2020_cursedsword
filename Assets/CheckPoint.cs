﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public ParticleSystem activationParticles;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Gamemanager.Instance.SetCheckpoint(this);
        }
    }

    private void Update()
    {
        if (Gamemanager.Instance.GetLastCheckpoint() == this)
        {
            if (!activationParticles.gameObject.activeInHierarchy)
            {
                AudioController.Instance.PlaySFXCheckPoint();
            }
            activationParticles.gameObject.SetActive(true);
        }
        else
        {
            activationParticles.gameObject.SetActive(false);
        }
    }
}
